import requests

def create_codenode_id():
    config = {
        "name": "indiagold_purity",
        "config": {
            "node_name": "indiagold_purity"
        },
        "description": "Model for indiagold purity detection"
    }
    url = "https://model-registry-staging.madstreetden.com/codenode/"
    # POST data to the above url
    response = requests.post(url, json=config, headers={'content-type': 'application/json'})
    print(response)
    if response.ok:
        print(response.json())

def create_graph():
    #graph create payload
    graph_payload = {
        "name": "indiagold_purity_detection_v0.6",
        "members": [
            {
                "node": {
                    "name": "indiagold_purity",
                    "attributes": [
                    ],
                    "deployment": {
                        "code_node": "7e42669a-d823-4a82-a99c-27a2b013c6c1",
                        "docker_image": "971037846030.dkr.ecr.us-east-1.amazonaws.com/nw-modules:nwm-indiagold_purity_0.8",
                        "deployment_data": [
                            {
                                "config": 2061
                            }
                        ]
                    }
                },
                "dependencies": [
                    "input"
                ]
            }
            

    ],
            
        "parent_graph_id": "",
        "region": "us-east-1",
        "description" : "indiagold purity detection model"
    }

    #create graph API
    url = "https://model-registry-staging.madstreetden.com/graph/"
    inf_url = "https://mlaas-interface-staging.madstreetden.com/api/v1/push"
    get_url = "https://mlaas-interface-staging.madstreetden.com/api/v1/"

    headers = {"content-type": "application/json"}
    res = requests.post(url, json=graph_payload, headers=headers)
    print(res)
    print(res.json())
    graph_id = res.json()["id"]
    print(graph_id)
    return graph_id

def deploy_graph(graph_id):
#     graph_id = "g-34fb5b74-4b70-4215-9251-2db94d95b82f"
    deploy_url = 'https://model-registry-staging.madstreetden.com/graph/{}/deploy/?transport={}'.format(graph_id, "http")

    res = requests.put(deploy_url, timeout=45)
    print(res)
    
    
def submit_payload(graph_id, item):
    # inference
    push_url = "https://mlaas-interface-staging.madstreetden.com/api/v1/push"
    data = {
    "payload": item,
    "graph": graph_id
    }
    res = requests.post(push_url, json=data)
    packet_id = res.json()["packet_id"]
    return packet_id,res
    
sample_payload  = {'process_object': {'meta_data': {'coordinates': [100, 80, 120, 80],                         
                    'image_url': 'https://msd-cvteam-apse.s3.ap-southeast-1.amazonaws.com/tarun/indiagold_training/f9a6c3ef-c018-4b76-b97e-5ebf774de1bb_479_232_491_547.png'}}}


def get_call(packet_id):
    url = "https://mlaas-interface-staging.madstreetden.com/api/v1/{}".format(packet_id)
    res = requests.get(url)
    return res.json()