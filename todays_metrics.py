import os 
import cv2
import subprocess
import json
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from tqdm import tqdm
import argparse
from copy import deepcopy
import pandas as pd
from glob import glob
from joblib import Parallel, delayed
from graph_utils import  *
from sklearn.metrics import classification_report, confusion_matrix


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--query_date', type=str, default=None,help='from date')
    parser.add_argument('--till_date', type=str, default=None,help='to date')
    args = parser.parse_args()
    os.system("aws s3 sync s3://india-gold/api_data/ api_data/")
    os.system(""" aws s3api list-objects-v2 --bucket "india-gold" --prefix "api_data" --query 'reverse(sort_by(Contents,&LastModified))' --output json > s3_counts.json""")
    with open("s3_counts.json", "r") as file:
            files = json.load(file)
    
    get_metrics(args.query_date, files, args.till_date)

def hit_graph(f, images):
    key = f.split('api_data/')[-1]
    coordinates = [int(x) for x in key.split('_')[1:-1]]
    fname = key.split('_')[0]
    image_name = [x for x in images if fname in x][0]
    image_url = "https://india-gold.s3.ap-southeast-1.amazonaws.com/{}".format(image_name)
    payload = {'process_object': {'meta_data': {'coordinates': coordinates,                         
                    'image_url': image_url}}}
    
    packet_id, response = submit_payload("g-1f0ff7bb-7010-41bd-8543-55e36ee71105", payload)
    return {'fname' : image_name,
                    'packet_id' : packet_id, 
                     'resp' : response.json()}

def get_metrics(query_date, a, till_date = None):
    feedback = []
    fnames = [x['Key'] for x in a]
    images = [x['Key'] for x in a if ".jpg" in x['Key']]
    for r in a:
        date = r['LastModified'].split('T')[0]
        date_range = pd.date_range(start=query_date, end= till_date)
        all_query_dates = [x.date().strftime("%Y-%m-%d") for x in date_range]
        # all_query_dates = ['2021-12-{}'.format(i) for i in range(int(query_date.split('-')[-1]), int(till_date.split('-')[-1]) +1)]
        if date in all_query_dates and "feedback" in r['Key']:
            feedback.append(r['Key'])
    
    responses = Parallel(n_jobs=12, backend="threading")(
            delayed(hit_graph)(f, images)
            for f in tqdm(feedback)
        )

    new_preds= []
    for resp in responses:
    #     print(resp['resp']['results']['indiagold_purity']['result']['result'])
        if resp['resp']['results']['indiagold_purity']['status'] == "STATUS_SUCCESS":
            new_preds.append({'fname': resp['fname'], 
                        'prediction': resp['resp']['results']['indiagold_purity']['result']['result'], 
                        'packet_id' : resp['packet_id']})
        else:
            print('ERROR')
    breakpoint()
    new_set = []
    for f in feedback:
        key = f.split('api_data/')[-1]
        fname = key.split('_')[0]
        coords = [int(x) for x in key.split('_')[1:-1]]
        image = "api_data/{}.jpg".format(fname)
    #     prediction = "_".join(f.split('_')[:-1]) + "_prediction.json"
        idx = [i for i in range(len(new_preds)) if new_preds[i]['fname'] == image][0]
        prediction = new_preds[idx]['prediction']
        assert f in fnames
        new_set.append({'image_path' : key,
                    'feedback' : f,
                    'prediction' : prediction})
        
    metrics_dict = []
    for x in new_set:
        key = x['image_path'].split('/')[-1].split('.jpg')[0]
        assert key in x['feedback']
        feedback_json = json.load(open(x['feedback']))
    #     prediction = json.load(open(x['prediction']))
        prediction = x['prediction']
        mapping_dict = {'15K' : '14K',
                        '14K' : '14K',
                        '16K' : '14K',
                        '17K' : '18K',
                        '18K' : '18K',
                        '19K' : '20K',
                        '20K' : '20K',
                        '21K' : '20K',
                        '22K' : '22K',
                        '23K' : "22K", 
                        '24K'  : "22K"}
                        
        pred = prediction[0][0]
        fb = feedback_json['label']
        fb_mapped = mapping_dict[fb]
        if pred in ['14K', '22K', '20K', '18K']:
            metrics_dict.append({'image_path' : x['image_path'], 
                        'prediction' : pred, 
                        'feedback' : fb, 
                        'feedback_mapped' : fb_mapped})
        
    
    df= pd.DataFrame(metrics_dict)
    df.to_csv('{}_{}_metrics.csv'.format(query_date, till_date), index = False)


if __name__ == "__main__":
    main()