import os 
import cv2
import json
import numpy as np
import matplotlib.pyplot as plt
import math
from tqdm.notebook import tqdm
from copy import deepcopy
import pandas as pd
from joblib import Parallel, delayed
import boto3
from boto3.s3.transfer import S3Transfer, TransferConfig
import argparse
from joblib import Parallel, delayed




class S3(object):

    def __init__(self, region="ap-southeast-1"):

        self.s3client = boto3.client('s3', region)
        self.s3resource = boto3.resource('s3')
        self.config = TransferConfig(use_threads=False)
        self.transfer = S3Transfer(self.s3client, self.config)

    def s3_upload(self, bin_obj, bucket, key, acl='bucket-owner-full-control', content_type=None,
                  meta_data={}, results_dict=None):
        """
        Upload object to an AWS S3 bucket.

        Parameters
        ----------
        bin_obj : object
            Object to be uploaded to S3.

        bucket : str
            Bucket to which to upload the object.

        key : str
            S3 key.

        acl : str
            Access control list.

        content_type : str

        meta_data : dict

        results_dict : dict

        Returns
        -------
        None
        """
        s3_res = self.s3client.put_object(
            Body=deepcopy(bin_obj),
            Bucket=bucket,
            Key=key,
            Metadata=meta_data,
            ContentType=content_type if content_type else '',
            ACL=acl if acl else '')
        if s3_res['ResponseMetadata']['HTTPStatusCode'] != 200:
            raise Exception(s3_res)

        if results_dict:
            results_dict[key] = True

    def upload_file_to_s3(self, loc_path, key, bucket,
                          filename=None,
                          extra_args={'ACL': 'bucket-owner-full-control'},
                          results_dict=None, get_error=False):
        """
        Uploads a file to S3.

        Parameters
        ----------
        loc_path : str

        bucket : str\n
        S3 bucket to which the file will be uploaded.

        key : str\n
        S3 key that will be used by boto3 to access the S3 bucket.

        filename : str, optional\n
        Name of the file to upload. If not provided, get the basename from imgfpath.

        extra_args : dict, optional

        results_dict : dict

        Returns
        -------
        {bool, None}
        """

        try:
            self.transfer.upload_file(loc_path, bucket, key, extra_args=extra_args)
            if results_dict:
                results_dict[loc_path] = [key, True]
            else:
                return True
        except Exception as e:
            if results_dict:
                results_dict[loc_path] = [key, False]
            else:
                if get_error:
                    return (False, e)
                else:
                    return False

def pad(x,y,a,b,pad_percent=10):
    if abs(x-a)>abs(y-b):
        major_axis = abs(x-a)
        minor_disp = (pad_percent*0.01)*major_axis
        y_new = y+minor_disp
        b_new = b-minor_disp
        return int(x),int(y_new),int(a),int(b_new)
    else:
        major_axis = abs(y-b)
        minor_disp = (pad_percent*0.01)*major_axis
        x_new = x-minor_disp
        a_new = a+minor_disp
        return int(x_new),int(y),int(a_new),int(b)
    
def rotate(origin, point, angle):
    ox, oy = origin
    px, py = point

    qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
    qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
    return int(qx), int(qy)

def rotate_get_new_points(img, points):
    x,y,a,b = points
    image_center = tuple(np.array(img.shape[1::-1]) / 2)
    try:
        slope = (y-b)/(x-a)
        theta = math.degrees(math.atan(slope))
    except:    
        theta = 90
    x_new,y_new = rotate(image_center, [x,y], math.radians(-theta))
    a_new,b_new = rotate(image_center, [a,b], math.radians(-theta))
    rot_mat = cv2.getRotationMatrix2D(image_center, theta, 1.0)
    result = cv2.warpAffine(img, rot_mat, img.shape[1::-1], flags=cv2.INTER_LINEAR)
    rotated_image = result.copy()
    x_pad,y_pad,a_pad,b_pad = pad(x_new,y_new,a_new,b_new)
    bottom,top = [y_pad,b_pad] if b_pad > y_pad else [b_pad,y_pad]
    left,right = [a_pad,x_pad] if x_pad > a_pad else [x_pad,a_pad]
    
    roi_orig = rotated_image[bottom:top,left:right]
    
    return roi_orig

def draw_bbox(img_path, shape, fname, path, vis_id, color= (0,0,255)):
    
    img = cv2.imread(os.path.join('api_data', img_path))
    img_scratch = img.copy()
    x1 , y1, x2, y2= shape
    img_copy = img.copy()
    img_copy = cv2.rectangle(img_copy, (x1, y1), (x2, y2), color, thickness= 2)

    save_path = os.path.join(path, "{}_bbox.png".format(fname))
    cv2.imwrite(save_path, img_copy)
    s3_fname_bbox = "{}/{}_bbox.png".format(vis_id, fname)
    s3_stat1 = s3_obj.upload_file_to_s3(save_path, "tarun/indiagold-train-vis/{}".format(s3_fname_bbox), bucket, extra_args={"ACL": "public-read", "ContentType": "image/jpeg"})
    assert s3_stat1
    os.remove(save_path)
 
    try:
        scratch = rotate_get_new_points(img_scratch, shape)
        save_path = os.path.join(path, "{}_scratch.png".format(fname))
        cv2.imwrite(save_path, scratch)
        s3_fname_scratch = "{}/{}_scratch.png".format(vis_id, fname)
        s3_stat = s3_obj.upload_file_to_s3(save_path, "tarun/indiagold-train-vis/{}".format(s3_fname_scratch), bucket, extra_args={"ACL": "public-read", "ContentType": "image/jpeg"})
        assert s3_stat
        os.remove(save_path)
    except:
        print(img_path)
        s3_fname_scratch = None
        
    return (s3_fname_bbox, s3_fname_scratch)

def df_gen():
    os.system(""" aws s3 sync s3://india-gold/api_data api_data/""")
    os.system(""" aws s3api list-objects-v2 --bucket "india-gold" --prefix "api_data" --query 'reverse(sort_by(Contents,&LastModified))' --output json > s3_counts.json""")
    with open("s3_counts.json", "r") as file:
            s3_files = json.load(file)

    feedback_files = [x['Key'] for x in s3_files if "feedback" in x['Key']]

    prediction_files = list()

    feedback_files = [x['Key'] for x in s3_files if "feedback" in x['Key']]
    for f in tqdm(feedback_files):
        pred_file = f.split('feedback')[0] + "prediction.json"
        if pred_file in [x['Key'] for x in s3_files if "prediction" in x['Key']]:
            prediction_files.append(pred_file)

    all_rows = list()
    for f, p in tqdm(zip(feedback_files, prediction_files)):
        feedback = json.load(open(f, "r"))
        prediction = json.load(open(p, "r"))
        coords = [int(x) for x in f.split('_')[2:-1]]
        try:
            feedback_img = [x['Key'].split('/')[-1] for x in s3_files if f.split('/')[-1].split('_')[0] in x['Key'] and "jpg" in x['Key']][0]
        except:
            print(f)
            continue

        image_url = "https://india-gold.s3.ap-southeast-1.amazonaws.com/api_data/{}".format(feedback_img)
        fb = feedback['label']
        pred = prediction['data']['results'][0][0]
        date = [x['LastModified'].split('T')[0] for x in s3_files if f == x['Key']][0]
        all_rows.append({"Image": feedback_img,
                        "image_url": image_url,
                        "bbox" : coords,
                        "date" : date,
                        "prediction" : pred,
                        "feedback" : fb,
                        "feedback_file" : f})
                        
    df = pd.DataFrame(all_rows)
    df['scratch_fname'] = df['feedback_file'].str.split('/').str[-1].str.split('_feedback').str[:-1].str[0]
    df['img_fname'] = df['image_url'].str.split('/').str[-1]
    df_lis = df.to_dict(orient = 'records')

    return df_lis, df

def gen_box_scratch_urls(df_lis, df, vis_id):
    box_responses = Parallel(n_jobs=12, backend="threading")(
                delayed(draw_bbox)(r['img_fname'], r['bbox'], r['scratch_fname'], save, vis_id)
                for r in tqdm(df_lis))

    box_urls = ["https://msd-cvteam-apse.s3.ap-southeast-1.amazonaws.com/tarun/indiagold-train-vis/{}".format(b[0]) for b in box_responses]
    scratch_urls = ["https://msd-cvteam-apse.s3.ap-southeast-1.amazonaws.com/tarun/indiagold-train-vis/{}".format(b[1]) 
                if b[1]
                else None
                for b in box_responses]
    df['box_url'] = box_urls
    df['scratch_url'] = scratch_urls

    return df


s3_region = "ap-southeast-1"
bucket = "msd-cvteam-apse"
# bucket = "india-gold"
s3_obj = S3(s3_region)
save = '/home/jovyan/data/indiagold/vis/'
img_path = "/home/jovyan/data/indiagold/api_data/"
save_path = '/home/jovyan/data/indiagold/vis/'


def main(path, vis_id):
    df_lis, df = df_gen()
    df = gen_box_scratch_urls(df_lis, df, vis_id)
    df.to_csv(path, index = False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default=None,help='path to save data')
    parser.add_argument('--vis_id', type=str, default=None,help='path to save data')
    args = parser.parse_args()
    main(args.path, args.vis_id)