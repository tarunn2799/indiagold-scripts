import os
import cv2
import json
import numpy as np
import torch
import random
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from tqdm import tqdm
import pandas as pd
from torchvision import transforms
import argparse
from glob import glob 

from joblib import Parallel, delayed
import sys

sys.path.append('/home/ubuntu/train_conv/')

from msd_utils.data_utils.data_sampling import Sample

mapping_dict = {"14_18_22" : {'15K' : '14K', 
                        '16K' : '14K',
                        '17K' : '18K',
                        '19K' : '18K',
                        '20K' : '18K',
                        '21K' : '22K',
                        '23K' : "22K", 
                        '24K'  : "22K"},
                "14_18_20_22" : {'15K' : '14K', 
                                '16K' : '14K',
                                '17K' : '18K',
                                '19K' : '20K',
                                '21K' : '20K',
                                '23K' : "22K", 
                                '24K'  : "22K"},
                "14_18_20_22_A" : {'15K' : '14K', 
                                '16K' : '14K',
                                '17K' : '18K',
                                '19K' : '18K',
                                '21K' : '20K',
                                '23K' : "22K", 
                                '24K'  : "22K"}}

t = transforms.Compose(
            [
                    transforms.RandomApply(
                    [
                        transforms.ColorJitter(hue=0.1),
                        transforms.GaussianBlur(kernel_size=(5, 9), sigma=(0.1, 5)),
                        transforms.RandomEqualize()
                    ],
                    p=0.3,
                ),
                transforms.RandomHorizontalFlip(p=0.6), 
                transforms.RandomApply([transforms.RandomAffine(degrees = 0, translate= (0.1, 0.1)),
                                       transforms.RandomRotation(degrees=(0, 5))], 0.6),
                
            ]
        )

def update_train_config(destination):
    with open("/data/indiagold/vector_train_config.json", "r") as file:
        vector_train_config = json.load(file)
    
    vector_train_config['data_dict']['input_csv_path'] = os.path.join(destination, "vector_train.csv")

    with open("/data/indiagold/vector_train_config.json", "w") as file:
        json.dump(vector_train_config, file, indent = 2)
    
    print("Train Config updated. Start your training run with \n debug=True TQDM=True RAY_INIT=manual python train_conv/train.py --train_config /data/indiagold/vector_train_config.json --user tarun")
    

def pil_to_cv2(img):
    pil_image = img.convert('RGB') 
    open_cv_image = np.array(pil_image) 
    # Convert RGB to BGR 
    open_cv_image = open_cv_image[:, :, ::-1].copy() 
    return open_cv_image

def cv2_to_pil(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img)
    return im_pil

def fname_to_bbox(fname):
    return [int(x) for x in fname.split('_')[1:-1]]


def slice_roi(line_coords, img, set_width = 10):
    cnt = np.array([
            line_coords[0],
            line_coords[1]
        ])
    rect = cv2.minAreaRect(cnt)
    rect_h = (rect[0], (rect[1][0], set_width), rect[2])

    rot_w, rot_h = rect_h[1]
    rot_w = int(rot_w)
    box = cv2.boxPoints(rect_h)
    box = np.int0(box)
    
    src_pts = box.astype("float32")
    dst_pts = np.array([[0, rot_h-1],
                        [0, 0],
                        [rot_w-1, 0],
                        [rot_w-1, rot_h-1]], dtype="float32")
    
    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    warped = cv2.warpPerspective(img, M, (rot_w, rot_h))
    return warped, box

def connected_components(mask_temp):
    nb_components, output, stats, centroids = cv2.connectedComponentsWithStats(mask_temp, connectivity=4)
    max_label, max_size = max([(i, stats[i, cv2.CC_STAT_AREA]) for i in range(1, nb_components)], key=lambda x: x[1])
    return output==max_label

def crop_rect(img, rect):

    center, size, angle = rect[0], rect[1], rect[2]
    center, size = tuple(map(int, center)), tuple(map(int, size))
    height, width = img.shape[0], img.shape[1]
    M = cv2.getRotationMatrix2D(center, angle, 1)
    img_rot = cv2.warpAffine(img, M, (width, height))
    img_crop = cv2.getRectSubPix(img_rot, size, center)

    return img_crop, img_rot

def crop_sandstone(img):
    
    img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret,thresh = cv2.threshold(img_gray, 95, 255,1)
    conn = connected_components(thresh)
    img_copy = img.copy()
    conn_2 = conn.astype(np.uint8)
    contours, hierarchy = cv2.findContours(conn_2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    c = max(contours, key = cv2.contourArea)
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    cnt = box
    rect = cv2.minAreaRect(cnt)
    img_crop, img_rot = crop_rect(img, rect)
    return img_crop, rect

def normalize(img, img_cropped):
    #img_crop_gray = cv2.cvtColor(img_crop, cv2.COLOR_BGR2GRAY)
    img_norm = np.zeros([img.shape[0],img.shape[1],3])

    mean, STD  = cv2.meanStdDev(img_cropped[:,:,0])

    clipped = np.clip(img[:,:,0], mean - STD, mean + STD).astype(np.uint8)

    temp_norm_r= cv2.normalize(clipped, clipped, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

    mean, STD  = cv2.meanStdDev(img_cropped[:,:,1])
    
    clipped = np.clip(img[:,:,1], mean - STD, mean + STD).astype(np.uint8)
    temp_norm_g= cv2.normalize(clipped, clipped, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)

    mean, STD  = cv2.meanStdDev(img_cropped[:,:,2])
    
    clipped = np.clip(img[:,:,2], mean - STD, mean + STD).astype(np.uint8)
    temp_norm_b= cv2.normalize(clipped, clipped, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)


    img_norm=np.stack((temp_norm_r,temp_norm_g,temp_norm_b),axis=2)
    
    return img_norm


def create_data_val(f, path, all_files, destination, s3_files):
    try:
        image_augs = []
        img = cv2.imread(f)
        cropped_img, _ = crop_sandstone(img)
        img = normalize(img, cropped_img)
        api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
        feedback_json = [x for x in api_files if "feedback" in x]
        for fj in feedback_json:
            img_name = [x['Key'].split('/')[-1] for x in s3_files if fj.split('_')[0] in x['Key'] and "jpg" in x['Key']][0]
            image_url = "https://india-gold.s3.ap-southeast-1.amazonaws.com/api_data/{}".format(img_name)
            label = json.load(open(os.path.join(path, fj), "r"))
            fname = "_".join(fj.split('_')[:-1])
            bbox = fname_to_bbox(fj)
            # x1, y1, x2, y2 = padding(bbox)
            x1, y1, x2, y2 = bbox
            line_coords = [(x1, y1), (x2, y2)]
            # scratch = img[min(y1, y2): max(y1, y2), min(x1, x2) : max(x1, x2)]
            pad_length = abs(x2 -x1) * 0.1 + abs(y2 - y1) if abs((x2- x1)) > abs((y2-y1)) else abs(y2 -y1) * 0.1 + abs(x2 -x1)
            scratch, src_pts = slice_roi(line_coords, img, set_width= int(pad_length))
            if scratch.shape[0] < 50 or scratch.shape[1] < 50:
                pass
            # print([x1,y1,x2,y2], bbox, scratch.shape, img.shape)
            img_arr = cv2.resize(scratch, (115, 330), interpolation = cv2.INTER_AREA)
            img_arr = img_arr.transpose(2, 0, 1)
            f_dup = fname.split('.')[0] + ".npy"
            save_path = '{}/{}'.format(destination,f_dup)
            created_date = get_date(f, s3_files)
            image_augs.append({"filename" : fname,
                                "vector_path" : save_path,
                                "labels" : label["label"], 
                                "status" : "val", 
                              "date" : created_date,
                              "bbox" : src_pts.tolist(),
                              "image_url": image_url,
                              "img_fname": img_name})
            with open(save_path, 'wb') as file:
                np.save(file, img_arr)
    except Exception as e:
        print("Error", e)

    return image_augs

def create_data(f, path, all_files, duplicate, destination, s3_files):
    #fetch all data
    try:
        image_augs = []
        img = cv2.imread(f)
        cropped_img, _ = crop_sandstone(img)
        img = normalize(img, cropped_img)
        api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
        feedback_json = [x for x in api_files if "feedback" in x]
        for fj in feedback_json:
            img_name = [x['Key'].split('/')[-1] for x in s3_files if fj.split('_')[0] in x['Key'] and "jpg" in x['Key']][0]
            image_url = "https://india-gold.s3.ap-southeast-1.amazonaws.com/api_data/{}".format(img_name)
            label = json.load(open(os.path.join(path, fj), "r"))
            fname = "_".join(fj.split('_')[:-1])
            bbox = fname_to_bbox(fj)
            # x1, y1, x2, y2 = padding(bbox)
            x1, y1, x2, y2 = bbox
            line_coords = [(x1, y1), (x2, y2)]
            # scratch = img[min(y1, y2): max(y1, y2), min(x1, x2) : max(x1, x2)]
            pad_length = abs(x2 -x1) * 0.1 + abs(y2 - y1) if abs((x2- x1)) > abs((y2-y1)) else abs(y2 -y1) * 0.1 + abs(x2 -x1)
            scratch, src_pts = slice_roi(line_coords, img, set_width= int(pad_length))
            scratch = (scratch * 255).astype('uint8')
            if scratch.shape[0] < 50 or scratch.shape[1] < 50:
                pass
            # print([x1,y1,x2,y2], bbox, scratch.shape, img.shape)
            # dup_dict = {"18K" : 2,
            #             "17K" : 2,
            #             "19K" : 2,
            #             "22K" : 2,
            #             "21K" : 2, 
            #             "14K" : 6,
            #             "15K" : 6, 
            #             "16K" : 6}
            # dup = dup_dict.get(label['label'], None)
            dup = duplicate
            img_pil = cv2_to_pil(scratch)
            for i in range(dup):
                transformed = t(img_pil)
                transformed = pil_to_cv2(transformed)
                img_arr = transformed / 255.
                img_arr = cv2.resize(img_arr, (115, 330), interpolation = cv2.INTER_AREA)
                img_arr = img_arr.transpose(2, 0, 1)
                f_dup = fname.split('.')[0] + "_{}.npy".format(i)
                save_path = '{}/{}'.format(destination,f_dup)
                created_date = get_date(f, s3_files)
                image_augs.append({"filename" : fname,
                                "vector_path" : save_path,
                                "labels" : label["label"], 
                                "status" : "train", 
                                "date" : created_date,
                                "bbox" : src_pts.tolist(),
                                "image_url": image_url,
                                "img_fname": img_name})
                with open(save_path, 'wb') as file:
                    np.save(file, img_arr)
    except Exception as e:
        print("Error", e)

    return image_augs
    
def get_labels_img(f, path, all_files, s3_files):
    labels = []
    api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
    feedback_json = [x for x in api_files if "feedback" in x]    
    for fj in feedback_json:
        label = json.load(open(os.path.join(path, fj), "r")).get('label')
        labels.append({'file_name' : f,
                       'label': label, 
                       'date' : get_date(f, s3_files)})
    return labels

def get_date(file, all_files):
    file = os.path.join('api_data', file.split('api_data/')[-1])
    for f in all_files:
        if f['Key'] == file:
            return f["LastModified"].split('T')[0]
    return None
    

def remove_date(date, files):
    files_for_date = []
    for f in files:
        file_date = f["LastModified"].split('T')[0]
        fname = f['Key']
        if file_date == date and ".jpg" in fname:
            files_for_date.append(fname)
    return files_for_date

def main(path, duplicate, train_dest, val_dest,mapping, val_csv, date = None):
    
    if not os.path.isdir(path):
        raise Exception("Folder doesn't exist")
    os.system("aws s3 sync s3://india-gold/api_data/ {}".format(path))
    os.system(""" aws s3api list-objects-v2 --bucket "india-gold" --prefix "api_data" --query 'reverse(sort_by(Contents,&LastModified))' --output json > s3_counts.json""")
    with open("s3_counts.json", "r") as file:
            s3_files = json.load(file)
    files = list(glob("{}/*.jpg".format(path)))

    
    if date:
        files_for_date = remove_date(date, s3_files)
        print('Num files for date' , files_for_date)
        print('Num files before : {}'.format(len(files)))
        files = [x for x in files if os.path.join('api_data', x.split('api_data/')[-1]) not in files_for_date]
        print('Num files after : {}'.format(len(files)))
        
    all_files = os.listdir(path)
    
    val_df = pd.read_csv(val_csv)
    val_files = val_df['file_name'].values
    train_files = [x for x in files if x.split('/')[-1] not in val_files]
    val_files = [os.path.join(path, x) for x in val_files]
    result = Parallel(n_jobs=12, backend="threading")(
        delayed(create_data)(f, path, all_files, duplicate, train_dest, s3_files)
        for f in tqdm(train_files)
    )

    result_val = Parallel(n_jobs=12, backend="threading")(
        delayed(create_data_val)(f, path, all_files, val_dest, s3_files)
        for f in tqdm(val_files)
    )

    image_augs = [item for sublist in result for item in sublist]
    image_augs_val = [item for sublist in result_val for item in sublist]
    image_augs.extend(image_augs_val)

    vector_df = pd.DataFrame(image_augs)
    vector_df['labels'] = vector_df['labels'].replace(mapping_dict.get(mapping, None))

    csv_save_path = os.path.join(train_dest, "vector_train.csv")
    vector_df.to_csv(csv_save_path, index = False)

    update_train_config(train_dest)
    
    print("Data stored in {} and \n {}".format(train_dest, val_dest))


# payload_data = {'process_object': {'meta_data': {'coordinates': [70, 80, 100, 80],
#                                    'image_url': 'https://msd-cvteam-apse.s3.ap-southeast-1.amazonaws.com/tarun/indiagold_training/f9a6c3ef-c018-4b76-b97e-5ebf774de1bb_479_232_491_547.png'}}}

# data = dict(packet_id="test-packet",resultset=dict(results=dict(input=dict(result=payload_data))))
# res = requests.post(url, json=data)

       
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default=None,help='path for data')
    parser.add_argument('--train_dest', type=str, default=None, help= 'path to store transformed images')
    parser.add_argument('--val_dest', type=str, default=None, help= 'path to store transformed images')
    parser.add_argument('--duplicate', type=int, default=None,help='number of times data should be replicated')
    parser.add_argument('--date', type=str, default=None,help='date to be removed')
    parser.add_argument('--val_csv', type=str, default=None,help='date to be removed')
    parser.add_argument('--mapping', type=str, default=None,help='label mapping')
    args = parser.parse_args()
    main(args.path, args.duplicate, args.train_dest, args.val_dest, args.mapping, args.val_csv, args.date)