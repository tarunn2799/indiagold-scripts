import os 
import cv2
import json
import numpy as np
import torch
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from tqdm import tqdm
import pandas as pd
from torchvision import transforms
import argparse
from glob import glob 

from joblib import Parallel, delayed



t = transforms.Compose(
            [
                    transforms.RandomApply(
                    [
                        transforms.ColorJitter(hue=0.1),
                        transforms.GaussianBlur(kernel_size=(5, 9), sigma=(0.1, 5)),
                        transforms.RandomEqualize()
                    ],
                    p=0.3,
                ),
                transforms.RandomHorizontalFlip(p=0.6), 
                transforms.RandomApply([transforms.RandomAffine(degrees = 0, translate= (0.1, 0.1)),
                                       transforms.RandomRotation(degrees=(0, 5))], 0.6),
                
            ]
        )
def update_train_config(destination):
    with open("/data/indiagold/vector_train_config.json", "r") as file:
        vector_train_config = json.load(file)
    
    vector_train_config['data_dict']['input_csv_path'] = os.path.join(destination, "vector_train.csv")

    with open("/data/indiagold/vector_train_config.json", "w") as file:
        json.dump(vector_train_config, file, indent = 2)
    
    print("Train Config updated. Start your training run with \n TQDM=True RAY_INIT=manual python train_conv/train.py --train_config /data/indiagold/vector_train_config.json --user tarun")
    

def pil_to_cv2(img):
    pil_image = img.convert('RGB') 
    open_cv_image = np.array(pil_image) 
    # Convert RGB to BGR 
    open_cv_image = open_cv_image[:, :, ::-1].copy() 
    return open_cv_image

def cv2_to_pil(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img)
    return im_pil

def fname_to_bbox(fname):
    return [int(x) for x in fname.split('_')[1:-1]]

def normalize(img):
    sandstone, (x,y,w,h) = crop_sandstone(img)
    norm_image = cv2.normalize(sandstone, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    img = np.float32(img)
    img[y:y+h, x:x+w] = norm_image
    return img

def padding(bbox, margin = 50):
    x1, y1, x2, y2 = min(bbox[0], bbox[2]), min(bbox[1], bbox[3]), max(bbox[0], bbox[2]), max(bbox[1], bbox[3])
    size = (115, 330) if abs(x2 - x1) < abs(y2-y1) else (330, 115)

    if abs(x2-x1) < 115:
        padding_area_x = abs(115 - (x2 - x1))
        padding_x = padding_area_x // 2
        x1 = x1 - padding_x
        x2 = x2 + padding_x

    if abs(y2 - y1) < 330:
        padding_area_y = abs(330 - (y2 - y1))
        padding_y = padding_area_y // 2
        y1 = y1 - padding_y
        y2 = y2 + padding_y
    
    return [x1, y1, x2, y2]

def crop_sandstone(img):
    img_color = img.copy()
    img_bw = img.copy()
    img_bw = cv2.cvtColor(img_bw, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(img_bw,(5,5),0)
    ret3,th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    mask = cv2.inRange(th3, 0, 250)
    contours,hierarchy = cv2.findContours(mask, 1, 2)
    c = max(contours, key = cv2.contourArea)
    x, y, w, h = cv2.boundingRect(c)
    cropped = img_color[y:y+h, x:x+w]
#     cropped = cv2.cvtColor(cropped, cv2.COLOR_BGR2RGB)
    return (cropped, (x, y, w, h))


def create_data(f, path, all_files, duplicate, destination):
    #fetch all data
    try:
        image_augs = []
        img = cv2.imread(f)
        img = normalize(img)
        api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
        feedback_json = [x for x in api_files if "feedback" in x]
        for fj in feedback_json:
            label = json.load(open(os.path.join(path, fj), "r"))
            fname = "_".join(fj.split('_')[:-1])
            bbox = fname_to_bbox(fj)
            x1, y1, x2, y2 = padding(bbox)
            scratch = img[min(y1, y2): max(y1, y2), min(x1, x2) : max(x1, x2)]
            scratch = (scratch * 255).astype('uint8')
            if scratch.shape[0] < 50 or scratch.shape[1] < 50:
                pass
            # print([x1,y1,x2,y2], bbox, scratch.shape, img.shape)
            dup_dict = {"18K" : 2, 
                        "22K" : 4,
                        "21K" : 4, 
                        "14K" : 6,
                        "15K" : 6, 
                        "16K" : 6}
            dup = dup_dict.get(label['label'], None)
            if not dup:
                dup = duplicate
            img_pil = cv2_to_pil(scratch)
            for i in range(dup):
                transformed = t(img_pil)
                transformed = pil_to_cv2(transformed)
                img_arr = transformed / 255.
                img_arr = cv2.resize(img_arr, (115, 330), interpolation = cv2.INTER_AREA)
                img_arr = img_arr.transpose(2, 0, 1)
                f_dup = fname.split('.')[0] + "_{}.npy".format(i)
                save_path = '{}/{}'.format(destination,f_dup)
                image_augs.append({"filename" : fname,
                                    "vector_path" : save_path,
                                    "labels" : label["label"]})
                with open(save_path, 'wb') as file:
                    np.save(file, img_arr)
    except Exception as e:
        print("Error", e)

    return image_augs
    
def main(path, duplicate, destination):
    if not os.path.isdir(path):
        raise Exception("Folder doesn't exist")
    os.system("aws s3 sync s3://india-gold/api_data/ {}".format(path))

    files = glob("{}/*.jpg".format(path))
    all_files = os.listdir(path)
# all_df_rows = []
    # for f in files:
    #     r = get_labels_img(f, path, all_files, s3_files)
    #     all_df_rows.extend(r)

    # all_df = pd.DataFrame(all_df_rows)
    # all_df['labels'] = all_df['label'].replace({'15K' : '14K', 
    #                                 '16K' : '14K',
    #                                 '17K' : '18K',
    #                                 '19K' : '18K',
    #                                 '20K' : '18K',
    #                                 '21K' : '22K',
    #                                 '23K' : "22K", 
    #                                 '24K'  : "22K"})
    
    # strat_obj = Sample(all_df)
    # strat_obj.stratified_sampling(column_name = "labels")
    # all_df = strat_obj.df
    # train_files = all_df[all_df['stratified_status'] == 'train']['file_name'].values
    # val_files = all_df[all_df['stratified_status'] == 'val']['file_name'].unique()
    result = Parallel(n_jobs=12, backend="threading")(
        delayed(create_data)(f, path, all_files, duplicate, destination)
        for f in tqdm(files)
    )

    image_augs = [item for sublist in result for item in sublist]

    vector_df = pd.DataFrame(image_augs)
    vector_df['labels'] = vector_df['labels'].replace({'15K' : '14K', 
                                    '16K' : '14K',
                                    '17K' : '18K',
                                    '19K' : '18K',
                                    '20K' : '18K',
                                    '21K' : '22K',
                                    '23K' : "22K", 
                                    '24K'  : "22K"})

    csv_save_path = os.path.join(destination, "vector_train.csv")
    vector_df.to_csv(csv_save_path, index = False)

    update_train_config(destination)
    
    print("Data stored in {}".format(destination))


# payload_data = {'process_object': {'meta_data': {'coordinates': [70, 80, 100, 80],
#                                    'image_url': 'https://msd-cvteam-apse.s3.ap-southeast-1.amazonaws.com/tarun/indiagold_training/f9a6c3ef-c018-4b76-b97e-5ebf774de1bb_479_232_491_547.png'}}}

# data = dict(packet_id="test-packet",resultset=dict(results=dict(input=dict(result=payload_data))))
# res = requests.post(url, json=data)

       
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default=None,help='path for data')
    parser.add_argument('--destination', type=str, default=None, help= 'path to store transformed images')
    parser.add_argument('--duplicate', type=int, default=None,help='number of times data should be replicated')
    args = parser.parse_args()
    main(args.path, args.duplicate, args.destination)