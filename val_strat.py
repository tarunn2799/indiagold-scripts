import os

from pandas.core.frame import DataFrame
import cv2
import json
import numpy as np
import torch
import random
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from tqdm import tqdm
import pandas as pd
from torchvision import transforms
import argparse
from glob import glob 

from joblib import Parallel, delayed
import sys

sys.path.append('/home/ubuntu/train_conv/')

from msd_utils.data_utils.data_sampling import Sample


mapping_dict = {"14_18_22" : {'15K' : '14K', 
                        '16K' : '14K',
                        '17K' : '18K',
                        '19K' : '18K',
                        '20K' : '18K',
                        '21K' : '22K',
                        '23K' : "22K", 
                        '24K'  : "22K"},
                "14_18_20_22" : {'15K' : '14K', 
                                '16K' : '14K',
                                '17K' : '18K',
                                '19K' : '20K',
                                '21K' : '20K',
                                '23K' : "22K", 
                                '24K'  : "22K"},
                "14_18_20_22_A" : {'15K' : '14K', 
                                '16K' : '14K',
                                '17K' : '18K',
                                '19K' : '18K',
                                '21K' : '20K',
                                '23K' : "22K", 
                                '24K'  : "22K"}}

def get_date(file, all_files):
    file = os.path.join('api_data', file.split('api_data/')[-1])
    for f in all_files:
        if f['Key'] == file:
            return f["LastModified"].split('T')[0]
    return None

def get_labels_img(f, path, all_files, s3_files):
    labels = []
    api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
    feedback_json = [x for x in api_files if "feedback" in x]
    for fj in feedback_json:
        label = json.load(open(os.path.join(path, fj), "r")).get('label')
        labels.append({'file_name' : f,
                       'labels': label, 
                       'date' : get_date(f, s3_files)})
    return labels

def remove_date(date, files):
    files_for_date = []
    for f in files:
        file_date = f["LastModified"].split('T')[0]
        fname = f['Key']
        if file_date == date and ".jpg" in fname:
            files_for_date.append(fname)
    return files_for_date

def update_image_paths(path, image_df):
    files = list(glob("{}/*.jpg".format(path))) 
    image_df = image_df.append(pd.DataFrame(files, columns = ['file_name']))
    image_df.drop_duplicates(subset=['file_name'], inplace = True)
    return image_df

def main(path, val_csv, image_csv, mapping=None, query_date= None, till_date=None):

    df = pd.read_csv(val_csv)
    val_folder = "/".join(val_csv.split('/')[:-1])
    val_csv_path = os.path.join(val_folder, "val_df_{}_{}.csv".format(mapping, till_date))
    if not os.path.isdir(path):
        raise Exception("Folder doesn't exist")
    os.system("aws s3 sync s3://india-gold/api_data/ {}".format(path))
    os.system(""" aws s3api list-objects-v2 --bucket "india-gold" --prefix "api_data" --query 'reverse(sort_by(Contents,&LastModified))' --output json > s3_counts.json""")
    with open("s3_counts.json", "r") as file:
            s3_files = json.load(file)
    # files = list(glob("{}/*.jpg".format(path), key=os.path.getmtime))

    image_df = pd.read_csv(image_csv)
    image_lis = update_image_paths(path, image_df)
    image_lis.to_csv(image_csv, index = False)
    files = image_lis['file_name'].values
    all_filtered_files = list()

    if query_date:
        # all_query_dates = ['2021-12-{}'.format(i) for i in range(int(query_date.split('-')[-1]), int(till_date.split('-')[-1]) +1)]
        date_range = pd.date_range(start=query_date, end= till_date)
        all_query_dates = [x.date().strftime("%Y-%m-%d") for x in date_range]
        for d in all_query_dates:
            files_for_date = remove_date(d, s3_files)
            print('Num files for date' , len(files_for_date))
            filtered_files = [x for x in files if os.path.join('api_data', x.split('api_data/')[-1]) in files_for_date]
            all_filtered_files.extend(filtered_files)
            print('Num files after : {}'.format(len(all_filtered_files)))
            
    all_files = os.listdir(path)
    all_df_rows = []
    for f in all_filtered_files:
        r = get_labels_img(f, path, all_files, s3_files)
        all_df_rows.extend(r)

    all_df = pd.DataFrame(all_df_rows)
    all_df['unmapped_labels'] = all_df['labels']
    if mapping:
        all_df['labels'] = all_df['labels'].replace(mapping_dict.get(mapping, None))

    strat_obj = Sample(all_df)
    strat_obj.stratified_sampling(column_name = "labels")
    all_df = strat_obj.df

    # all_df['file_name'] = all_df['file_name'].str.split('/').str[-2:].apply('/'.join)
    all_df['file_name'] = all_df['file_name'].str.split('/').str[-1]
    val_df = all_df[all_df['stratified_status'] == "val"]
    val_df.drop_duplicates(subset=['file_name'], inplace = True)

    df = df.append(val_df)
    df.drop_duplicates(subset=['file_name'], inplace = True)
    df.to_csv(val_csv_path, index = False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default=None,help='path for data (pass absolute path of api_data here)')
    parser.add_argument('--val_csv', type=str, default=None,help='pass the latest csv from /data/indiagold/validation_csv')
    parser.add_argument('--query_date', type=str, default=None,help='start date for data that we want')
    parser.add_argument('--till_date', type=str, default='2021-1-31',help='end date for data that we want')
    parser.add_argument('--mapping', type=str, default=None,help='label mapping refer to the mapping dict above for keys')
    parser.add_argument('--image_csv', type= str, default = None, help= 'maintains order for images. pass the image_df.csv file in data/indiagold here')
    args = parser.parse_args()
    main(args.path, args.val_csv, args.image_csv, args.mapping, args.query_date, args.till_date)

    








