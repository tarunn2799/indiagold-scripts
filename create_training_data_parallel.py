import os
import cv2
import json
import numpy as np
import torch
import random
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw
from tqdm import tqdm
import pandas as pd
from torchvision import transforms
import argparse
from glob import glob 

from joblib import Parallel, delayed
import sys

sys.path.append('/home/ubuntu/train_conv/')

from msd_utils.data_utils.data_sampling import Sample

mapping_dict = {"14_18_22" : {'15K' : '14K', 
                        '16K' : '14K',
                        '17K' : '18K',
                        '19K' : '18K',
                        '20K' : '18K',
                        '21K' : '22K',
                        '23K' : "22K", 
                        '24K'  : "22K"},
                "14_18_20_22" : {'15K' : '14K', 
                                '16K' : '14K',
                                '17K' : '18K',
                                '19K' : '20K',
                                '21K' : '20K',
                                '23K' : "22K", 
                                '24K'  : "22K"},
                "14_18_20_22_A" : {'15K' : '14K', 
                                '16K' : '14K',
                                '17K' : '18K',
                                '19K' : '18K',
                                '21K' : '20K',
                                '23K' : "22K", 
                                '24K'  : "22K"}}


t = transforms.Compose(
            [
                    transforms.RandomApply(
                    [
                        transforms.ColorJitter(hue=0.1),
                        transforms.GaussianBlur(kernel_size=(5, 9), sigma=(0.1, 5)),
                        transforms.RandomEqualize()
                    ],
                    p=0.3,
                ),
                transforms.RandomHorizontalFlip(p=0.6), 
                transforms.RandomApply([transforms.RandomAffine(degrees = 0, translate= (0.1, 0.1)),
                                       transforms.RandomRotation(degrees=(0, 5))], 0.6),
                
            ]
        )
def update_train_config(destination):
    with open("/data/indiagold/vector_train_config.json", "r") as file:
        vector_train_config = json.load(file)
    
    vector_train_config['data_dict']['input_csv_path'] = os.path.join(destination, "vector_train.csv")

    with open("/data/indiagold/vector_train_config.json", "w") as file:
        json.dump(vector_train_config, file, indent = 2)
    
    print("Train Config updated. Start your training run with \n debug=True TQDM=True RAY_INIT=manual python train_conv/train.py --train_config /data/indiagold/vector_train_config.json --user tarun")
    

def pil_to_cv2(img):
    pil_image = img.convert('RGB') 
    open_cv_image = np.array(pil_image) 
    # Convert RGB to BGR 
    open_cv_image = open_cv_image[:, :, ::-1].copy() 
    return open_cv_image

def cv2_to_pil(img):
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img)
    return im_pil

def fname_to_bbox(fname):
    return [int(x) for x in fname.split('_')[1:-1]]

def normalize(img):
    sandstone, (x,y,w,h) = crop_sandstone(img)
    norm_image = cv2.normalize(sandstone, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    img = np.float32(img)
    img[y:y+h, x:x+w] = norm_image
    return img

def padding(bbox, size= (115, 330)):
    area = size[0] * size[1]


def padding(bbox,margin = 50):
    #assuming bbox is in format : x1, y1, x2, y2
    x1, y1, x2, y2 = min(bbox[0], bbox[2]), min(bbox[1], bbox[3]), max(bbox[0], bbox[2]), max(bbox[1], bbox[3])
    if (x2 - x1) < margin or (y2-y1) < margin:
        padding_x, padding_y = (50, 20) if (x2 - x1) < (y2 - y1) else (20, 50)
        x1 = x1 - padding_x
        y1 = y1 - padding_y
        x2 = x2 + padding_x
        y2 = y2 + padding_y
        
    new_bbox = [x1, y1, x2, y2]
    for i in range(len(new_bbox)):
        if new_bbox[i] < 0:
            new_bbox[i] = 0
    return new_bbox

def crop_sandstone(img):
    img_color = img.copy()
    img_bw = img.copy()
    img_bw = cv2.cvtColor(img_bw, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(img_bw,(5,5),0)
    ret3,th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    mask = cv2.inRange(th3, 0, 250)
    contours,hierarchy = cv2.findContours(mask, 1, 2)
    c = max(contours, key = cv2.contourArea)
    x, y, w, h = cv2.boundingRect(c)
    cropped = img_color[y:y+h, x:x+w]
#     cropped = cv2.cvtColor(cropped, cv2.COLOR_BGR2RGB)
    return (cropped, (x, y, w, h))

def create_data_val(f, path, all_files, destination, s3_files):
    try:
        image_augs = []
        img = cv2.imread(f)
        img = normalize(img)
        api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
        feedback_json = [x for x in api_files if "feedback" in x]
        for fj in feedback_json:
            label = json.load(open(os.path.join(path, fj), "r"))
            fname = "_".join(fj.split('_')[:-1])
            bbox = fname_to_bbox(fj)
            x1, y1, x2, y2 = padding(bbox)
            scratch = img[min(y1, y2): max(y1, y2), min(x1, x2) : max(x1, x2)]
            if scratch.shape[0] < 50 or scratch.shape[1] < 50:
                pass
            # print([x1,y1,x2,y2], bbox, scratch.shape, img.shape)
            img_arr = cv2.resize(scratch, (115, 330), interpolation = cv2.INTER_AREA)
            img_arr = img_arr.transpose(2, 0, 1)
            f_dup = fname.split('.')[0] + ".npy"
            save_path = '{}/{}'.format(destination,f_dup)
            created_date = get_date(f, s3_files)
            image_augs.append({"filename" : fname,
                                "vector_path" : save_path,
                                "labels" : label["label"], 
                                "status" : "val", 
                              "date" : created_date})
            with open(save_path, 'wb') as file:
                np.save(file, img_arr)
    except Exception as e:
        print("Error", e)

    return image_augs

def create_data(f, path, all_files, duplicate, destination, s3_files):
    #fetch all data
    try:
        image_augs = []
        img = cv2.imread(f)
        img = normalize(img)
        api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
        feedback_json = [x for x in api_files if "feedback" in x]
        for fj in feedback_json:
            label = json.load(open(os.path.join(path, fj), "r"))
            fname = "_".join(fj.split('_')[:-1])
            bbox = fname_to_bbox(fj)
            x1, y1, x2, y2 = padding(bbox)
            scratch = img[min(y1, y2): max(y1, y2), min(x1, x2) : max(x1, x2)]
            scratch = (scratch * 255).astype('uint8')
            if scratch.shape[0] < 50 or scratch.shape[1] < 50:
                pass
            # print([x1,y1,x2,y2], bbox, scratch.shape, img.shape)
            
            dup = duplicate
            img_pil = cv2_to_pil(scratch)
            for i in range(dup):
                transformed = t(img_pil)
                transformed = pil_to_cv2(transformed)
                img_arr = transformed / 255.
                img_arr = cv2.resize(img_arr, (115, 330), interpolation = cv2.INTER_AREA)
                img_arr = img_arr.transpose(2, 0, 1)
                f_dup = fname.split('.')[0] + "_{}.npy".format(i)
                save_path = '{}/{}'.format(destination,f_dup)
                created_date = get_date(f, s3_files)
                image_augs.append({"filename" : fname,
                                    "vector_path" : save_path,
                                    "labels" : label["label"],
                                    "status" : "train", 
                                  "date" : created_date})
                with open(save_path, 'wb') as file:
                    np.save(file, img_arr)
    except Exception as e:
        print("Error", e)

    return image_augs
    
def get_labels_img(f, path, all_files, s3_files):
    labels = []
    api_files = [x for x in all_files if f.split('/')[-1].split('.')[0] in x]
    feedback_json = [x for x in api_files if "feedback" in x]    
    for fj in feedback_json:
        label = json.load(open(os.path.join(path, fj), "r")).get('label')
        labels.append({'file_name' : f,
                       'label': label, 
                       'date' : get_date(f, s3_files)})
    return labels

def get_date(file, all_files):
    file = os.path.join('api_data', file.split('api_data/')[-1])
    for f in all_files:
        if f['Key'] == file:
            return f["LastModified"].split('T')[0]
    return None
    

def remove_date(date, files):
    files_for_date = []
    for f in files:
        file_date = f["LastModified"].split('T')[0]
        fname = f['Key']
        if file_date == date and ".jpg" in fname:
            files_for_date.append(fname)
    return files_for_date

def main(path, duplicate, train_dest, val_dest,mapping, val_csv, date = None):
    
    if not os.path.isdir(path):
        raise Exception("Folder doesn't exist")
    os.system("aws s3 sync s3://india-gold/api_data/ {}".format(path))
    os.system(""" aws s3api list-objects-v2 --bucket "india-gold" --prefix "api_data" --query 'reverse(sort_by(Contents,&LastModified))' --output json > s3_counts.json""")
    with open("s3_counts.json", "r") as file:
            s3_files = json.load(file)
    files = list(glob("{}/*.jpg".format(path)))

    
    if date:
        files_for_date = remove_date(date, s3_files)
        print('Num files for date' , files_for_date)
        print('Num files before : {}'.format(len(files)))
        files = [x for x in files if os.path.join('api_data', x.split('api_data/')[-1]) not in files_for_date]
        print('Num files after : {}'.format(len(files)))
        
    all_files = os.listdir(path)
    
    val_df = pd.read_csv(val_csv)
    val_files = val_df['file_name'].values
    train_files = [x for x in files if x.split('/')[-1] not in val_files]
    val_files = [os.path.join(path, x) for x in val_files]
    result = Parallel(n_jobs=12, backend="threading")(
        delayed(create_data)(f, path, all_files, duplicate, train_dest, s3_files)
        for f in tqdm(train_files)
    )

    result_val = Parallel(n_jobs=12, backend="threading")(
        delayed(create_data_val)(f, path, all_files, val_dest, s3_files)
        for f in tqdm(val_files)
    )

    image_augs = [item for sublist in result for item in sublist]
    image_augs_val = [item for sublist in result_val for item in sublist]
    image_augs.extend(image_augs_val)

    vector_df = pd.DataFrame(image_augs)
    vector_df['labels'] = vector_df['labels'].replace(mapping_dict.get(mapping, None))

    csv_save_path = os.path.join(train_dest, "vector_train.csv")
    vector_df.to_csv(csv_save_path, index = False)

    update_train_config(train_dest)
    
    print("Data stored in {} and \n {}".format(train_dest, val_dest))


# payload_data = {'process_object': {'meta_data': {'coordinates': [70, 80, 100, 80],
#                                    'image_url': 'https://msd-cvteam-apse.s3.ap-southeast-1.amazonaws.com/tarun/indiagold_training/f9a6c3ef-c018-4b76-b97e-5ebf774de1bb_479_232_491_547.png'}}}

# data = dict(packet_id="test-packet",resultset=dict(results=dict(input=dict(result=payload_data))))
# res = requests.post(url, json=data)

       
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default=None,help='path for data')
    parser.add_argument('--train_dest', type=str, default=None, help= 'path to store transformed images')
    parser.add_argument('--val_dest', type=str, default=None, help= 'path to store transformed images')
    parser.add_argument('--duplicate', type=int, default=None,help='number of times data should be replicated')
    parser.add_argument('--date', type=str, default=None,help='date to be removed')
    parser.add_argument('--val_csv', type=str, default=None,help='date to be removed')
    parser.add_argument('--mapping', type=str, default=None,help='label mapping')
    args = parser.parse_args()
    main(args.path, args.duplicate, args.train_dest, args.val_dest, args.mapping, args.val_csv, args.date)