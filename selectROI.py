import json 
import numpy as np
import os
import cv2
import matplotlib.pyplot as plt
import imutils
import argparse

#Plot the diagonal - Testing
# color = (0, 255, 0)
# thickness = 5
# image = cv2.line(img, line_coords[0], line_coords[1], color, thickness)
# image = cv2.circle(img, line_coords[0], 25, (0,0, 255), -1)
# image = cv2.circle(image, line_coords[1], 25, (0,255, 0), -1)
# cv2.drawContours(img, [box], 0, (0, 0, 255), 20)

def slice_roi(line_coords, img):
    cnt = np.array([
            line_coords[0],
            line_coords[1]
        ])

    rect = cv2.minAreaRect(cnt)
    set_width = 150
    rect_h = (rect[0], (rect[1][0], set_width), rect[2])
    rot_w, rot_h = rect_h[1]
    rot_w = int(rot_w)
    box = cv2.boxPoints(rect_h)
    box = np.int0(box)

    src_pts = box.astype("float32")
    dst_pts = np.array([[0, rot_h-1],
                        [0, 0],
                        [rot_w-1, 0],
                        [rot_w-1, rot_h-1]], dtype="float32")


    M = cv2.getPerspectiveTransform(src_pts, dst_pts)
    warped = cv2.warpPerspective(img, M, (rot_w, rot_h))
    return warped

def generate_slices(warped):
    slices = []
    split = [int(x) for x in list(np.linspace(0, warped.shape[1], 10))]
    x_coords = [(split[i], split[i+1]) for i in range(len(split)-1)]
    for start, end in x_coords:
        slices.append(warped[0:warped.shape[0], start: end])
    return slices

def plot_hist(slices):
    fig, axs = plt.subplots(len(slices), 2, figsize= (20,20))
    for i in range(len(slices)):
        image = slices[i]
        plt.setp(axs[-1, :], xlabel='Bins')
        plt.setp(axs[:, 0], ylabel='# of Pixels')
        for j in range(2):
            if j%2 == 0:
                # axs[i, j].xlabel("Bins")
                # axs[i, j].ylabel("# of Pixels")
                chans = cv2.split(image)
                colors = ("b", "g", "r")
                for (chan, color) in zip(chans, colors):
                    hist = cv2.calcHist([chan], [0], None, [256], [0, 256])
                    axs[i, j].plot(hist, color=color)
                    # axs[i, j].xlim([0, 256])
            else:
                axs[i, j].axis("off")
                axs[i, j].imshow(imutils.opencv2matplotlib(image))

    plt.show()

def save_data(slices, fname):
    label = fname.split('/')[-2]
    img_name = fname.split('/')[-1].split('.')[0]

    avg_slices = [np.mean(slice, axis = 1) for slice in slices]
    slice_arr = np.array(avg_slices)
    slice_arr = slice_arr.flatten()
    if not os.path.isdir('/Users/tarun/Documents/msd/indiagold/dummy_data/{}'.format(label)):
        os.makedirs('/Users/tarun/Documents/msd/indiagold/dummy_data/{}'.format(label))
    outfile = '/Users/tarun/Documents/msd/indiagold/dummy_data/{}/{}.npy'.format(label, img_name)
    np.save(outfile, slice_arr)
    print('File Saved as {}'.format(outfile))



if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--image', type=str, help='path of the image')
    parser.add_argument('--mode', type=str, help = 'output of the script')
    parser.add_argument('--rotate', type=bool, help = 'output of the script')
    args = parser.parse_args()
    print(args.image)
    img = cv2.imread(args.image)
    if args.rotate:
        img = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    print(img.shape)

    fromCenter = False
    ROIs = cv2.selectROIs('Select ROIs', img, fromCenter)
    assert len(ROIs) == 1
    cv2.waitKey(0)
    # cv2.destroyAllWindows()
    x1, y1, w, h = ROIs[0]
    print('roi', ROIs[0])
    # x1, y1, w, h = [571, 1614 , 531 , 962]
    x2 = x1 + w
    y2 = y1 + h

    orientation = input('Enter diagonal orientation [tr, tl, c] : ')

    if orientation == 'tl': 
        line_coords = [(x1, y1), (x2, y2)]
        print(line_coords)

    elif orientation == 'tr':
        line_coords = [(x2 , y1), (x1, y2)]

    elif orientation == 'c':
        midpoint = (x1 + x2)// 2 
        line_coords = [(midpoint, y1), (midpoint, y2)]


    warped = slice_roi(line_coords, img)
    slices = generate_slices(warped)


    if args.mode == 'hist':
        plot_hist(slices)
    elif args.mode == 'save':
        save_data(slices, args.image)