import os 
import argparse
import json
import pandas as pd
from glob import glob

mapping_dict = { '14K' : '14K',
    '15K' : '14K', 
'16K' : '14K',
'17K' : '14K',
'18K' : '18K',
'19K' : '18K',
'20K' : '18K',
'21K' : '22K',
'22K' : '22K',
'23K' : "22K", 
'24K'  : "22K"}

def gen_export_link(link):
    c = link.split('/')
    x = c[-2]
    y = c[-1].split('gid=')[-1]
    csv_link = 'https://docs.google.com/spreadsheets/d/{}/export?gid={}&format=csv'.format(x,y)
    return csv_link

def map_labels(row):
    gt = mapping_dict[row['feedback']]
    vm1 = mapping_dict[row['label_vm1']]
    vm3 = mapping_dict[row['label_vm3']]
    
    return gt, vm1, vm3

def main(vm1_sheet, vm3_sheet, bt_sheet):

    vm1 = pd.read_csv(gen_export_link(vm1_sheet))
    vm3 = pd.read_csv(gen_export_link(vm3_sheet))
    bt = pd.read_csv(os.path.join('blind_tests', bt_sheet))
    vm1.rename(columns = {'label': 'label_vm1'}, inplace = True)
    vm3.rename(columns = {'label': 'label_vm3'}, inplace = True)
    vm = pd.merge(vm1, vm3[['scratch_fname', 'label_vm3']], on = 'scratch_fname', how = 'left')
    gt_pred = pd.merge(bt, vm[['scratch_fname' , 'label_vm1', 'label_vm3']], on = 'scratch_fname', how = 'left')
    gt_pred = gt_pred[gt_pred['label_vm1'].notna()]
    gt_pred[['feedback_mapped','vm1_mapped','vm3_mapped']] = gt_pred.apply(map_labels, axis = 1, result_type = "expand")

    gt_pred.loc[gt_pred['vm1_mapped'] == gt_pred['vm3_mapped'], 'vm1vsvm3'] = "MATCH"
    gt_pred.loc[gt_pred['vm1_mapped'] != gt_pred['vm3_mapped'], 'vm1vsvm3'] = "NOT A MATCH"
    gt_pred.loc[gt_pred['vm1_mapped'] == gt_pred['feedback_mapped'], 'vm1vsfeedback'] = "MATCH"
    gt_pred.loc[gt_pred['vm1_mapped'] != gt_pred['feedback_mapped'], 'vm1vsfeedback'] = "NOT A MATCH"
    gt_pred.loc[gt_pred['vm3_mapped'] == gt_pred['feedback_mapped'], 'vm3vsfeedback'] = "MATCH"
    gt_pred.loc[gt_pred['vm3_mapped'] != gt_pred['feedback_mapped'], 'vm3vsfeedback'] = "NOT A MATCH"
    save_path = os.path.join('blind_tests/results', '{}_results.csv'.format(bt_sheet.split('.')[0]))
    print(save_path)
    gt_pred.to_csv(save_path, index = False)
    
    





if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--vm1_sheet', type=str, default=None,help='vm1 sheet')
    parser.add_argument('--vm3_sheet', type=str, default=None,help='vm3 sheet')
    parser.add_argument('--bt_sheet', type= str, default = None, help = 'blind test sheet')
    args = parser.parse_args()
    main(args.vm1_sheet, args.vm3_sheet, args.bt_sheet)