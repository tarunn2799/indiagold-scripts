import requests
import json


# urls
ram = "https://hooks.slack.com/services/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"
xblox_tech = "https://hooks.slack.com/services/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"
xblox_algo = "https://hooks.slack.com/services/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"
xblox_product_dev = "https://hooks.slack.com/services/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"
tarun = "https://hooks.slack.com/services/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"
india_gold_internal = "https://hooks.slack.com/services/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"
# message
standup_message = "IndiaGold Counts : \n"


def send_message(hook_url, message, user_ids, new_message=True):
    text = ' '.join(["<@{}>".format(n) for n in user_ids])
    if new_message:
        res = requests.post(hook_url, data=json.dumps({"text": standup_message.format(message, text)}))
    else:
        res = requests.post(hook_url, data=json.dumps({"text": "{} {}".format(message, text)}))
    return res


# xblox tech
pth = tarun
user_ids = []
#user_ids = ['UP8TW9GBG', 'U012ASQ0CDN', 'U021R5EEREV']
#user_names = ['vishnu', 'Chirag', 'adithya prem anand']  # just for visibility
send_message(pth, "Xblox Algo/Infra and trainconv thread", user_ids)

