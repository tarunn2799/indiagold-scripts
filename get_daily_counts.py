import os
import json
import datetime
import dateutil.parser
import pandas as pd
from copy import deepcopy

def parse_date(row):
    d = dateutil.parser.parse(row['LastModified'])
    date = pd.to_datetime(d, infer_datetime_format=True)
    return date

def set_count(row):
    if "feedback" in row["Key"]:
        return True

def date_to_str(row):
    if "feedback" in row["Key"]:
        return row["datetime"].strftime('%d/%m/%Y')

def get_caratage(row):
    feedback  = row["Key"]
    with open(feedback, "r") as file:
        feedback_data = json.load(file)
    return feedback_data['label']


if __name__ == "__main__":
    print("Running S3 Sync with api_data/")
    os.system(""" aws s3 sync s3://india-gold/api_data /home/jovyan/data/indiagold/api_data/ """)
    os.system(""" aws s3api list-objects-v2 --bucket "india-gold" --prefix "api_data" --query 'reverse(sort_by(Contents,&LastModified))' --output json > indiagold-scripts/s3_counts.json""")
    with open("indiagold-scripts/s3_counts.json", "r") as file:
        a = json.load(file)

    df = pd.DataFrame(a)

    df["datetime"] = df.apply(parse_date, axis = 1)
    df["counts"] = df.apply(set_count, axis = 1)
    df["Date"] = df.apply(date_to_str, axis = 1)
    df = df[df["Date"].notna()]
    df["label"] = df.apply(get_caratage, axis =1)
    df_caratage = deepcopy(df)
    df_caratage = pd.DataFrame(df_caratage.groupby(by = ["Date", "label"]).size()).rename({0 : 'counts'}, axis = 1)
    print(df["Date"].value_counts())
    date_range = pd.date_range(start="2022-01-01", end= "2022-12-01")
    all_query_dates = [x.date().strftime("%d/%m/%Y") for x in date_range]
    df_caratage = df_caratage.query("Date in @all_query_dates")
    df_caratage.to_csv('/home/jovyan/data/indiagold/daily_counts_2022.csv')
    

