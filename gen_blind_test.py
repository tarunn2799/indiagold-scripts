import os 
import argparse
import json
import pandas as pd
from glob import glob

def fetch_exist(csvs):
    img_fnames = []
    for c in csvs:
        d = pd.read_csv(c)
        img_fnames.extend(list(d['img_fname'].unique()))
    return img_fnames

def main(path, date, limit):

    csvs = glob('blind_tests/*.csv')
    exist_imgs = fetch_exist(csvs)
    
    df = pd.read_csv(path)
    print('Loaded all data')
    df = df[~df['img_fname'].isin(exist_imgs)]
    all_imgs = df['img_fname'].unique()
    rows = list()
    for x in all_imgs:
        sub_df = df[df['img_fname'] == x].to_dict('records')
        rows.extend(sub_df)
        if len(rows) > limit:
            break
    sample_df = pd.DataFrame(rows) 
    sample_df['label_absolute'] = sample_df['feedback'].str[:2]

    sample_df.to_csv('blind_tests/blind_test_{}.csv'.format(date), index = False)
    print('Saved sample to {}'.format('blind_tests/blind_test_{}'.format(date)))

    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', type=str, default=None,help='path to all data')
    parser.add_argument('--date', type=str, default=None,help='date of qa')
    parser.add_argument('--limit', type= int, default= 100, help = 'num images sampled')
    args = parser.parse_args()
    main(args.path, args.date, args.limit)
